<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<h1 class="text-primary">Простая форма</h1>

{!! Form::open(array('url' => '/form', 'method' => 'post'))!!}
<div class="col-xs-3">
<div class="form-group ">
    {!! Form::label('name','Name:') !!}
    {!! Form::text('name',null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('lastname','Lastname:') !!}
    {!! Form::text('lastname',null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('body','Your text: ') !!}
    {!! Form::textarea('body',null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::submit('Send Form',['class'=>'btn btn-primary ']) !!}
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
</div>
</div>

{!! Form::close() !!}
