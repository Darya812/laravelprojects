<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Log;

class FormController extends Controller
{
    public function index() {
        return view('first');
    }
    public function send() {
        $name = Input::get('name');
        $lastname = Input::get('lastname');
        $text = Input::get('body');
        Log::info('Form posted successfully!', ['name' => $name,'lastname'=>$lastname,'text'=>$text]);
        return redirect()->action('FormController@index')->with('message','Form posted in Log!');
    }

}
